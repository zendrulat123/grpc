package main

import (
	"fmt"
	"gitlab.com/zendrulat123/grpc/greet/greetpb"
	"google.golang.org/grpc"
	"log"
)

func main() {
	fmt.Println("works client")

	cc, err := grpc.Dial("localhost:50051", grpc.WithInsecure())
	if err != nil {
		log.Fatalf("client could not connect %v", err)
	}

	defer cc.Close()

	c := greetpb.NewGreetServiceClient(cc)

	fmt.Println("client created", c)
}
